package cs431.p3.ai;

import java.util.Arrays;


public class Team2AI extends Connect4AI 
{
	private Node head;
	private int[][] board;
	private int depthLimit = 4;
	private int depthCounter = 7;
	private AlphaBetaPruning abp = new AlphaBetaPruning();
	private int moves;
	private boolean player1;
	private static int WIDTH = 7;
	private static int HEIGHT = 6;
	public Team2AI(boolean player1) throws Exception 
	{
		this.player1 = player1;
		moves = 0; 
		this.board = new int[WIDTH][HEIGHT];
	}

	@Override
	public int getMove(int[][] board, boolean currentPlayer) 
	{
		//increasing depth limit as time goes on
		moves++;
		if(moves%depthCounter == 0) {
			depthLimit++;
		}
		
		//need to flip the board so that the algorithm/tree generator will work correctly
		int[][] tmp = new int[board.length][board[0].length];
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board[0].length;j++) {
				tmp[i][j] = board[i][board[0].length-j-1];
			}
		}
		for(int x = 0; x < board.length; x++)
		{
			for(int y = 0; y < board[0].length; y++)
			{
				if(this.board[x][y] != tmp[x][y])
				{
					this.board[x][y] = tmp[x][y];
				}
				System.out.print(this.board[x][y]);
			}
			System.out.println();
		}
		
		//generate the tree, run the alpha beta pruning alg, and return the column of the most optimal node
		generateTree(null,-1,-1,currentPlayer,0);
		Node c = abp.alphaBeta(head, Integer.MIN_VALUE, Integer.MAX_VALUE, AlphaBetaPruning.MAX);
		System.out.println(c);
		return c.getX();
	}
	
	/**
 	 * 
 	 * @param parent Parent node for the current branch (null if head node)
 	 * @param x The x coordinate of the board to be modified (-1 if head node)
 	 * @param y The y coordinate of the board to be modified (-1 if head node)
 	 * @param player True for player 1, false for player 2
 	 * @author Josh
 	 */
 	public void generateTree(Node parent, int x, int y, boolean player, int depth) {
 		Node cur;
 		
 		//IF parent is null, we are the head node. Start with a new board and tree. 
 		if(parent == null) {
 			cur = new Node();
 			head = cur;
 			x = 0; 
 			y = 0;
 		}
 		else { //Otherwise we need to modify our assigned location in the board and set ourselves as a child to the parent.
 			cur = new Node(parent);
 			parent.setChild(cur); 
 			cur.setPosition(x,y);
 			board[x][y] = player ? 1 : 2;
 		}
 		
 		//If the game is over, our branch is done so we return up the tree. 
 		if(isGameOver(x, y,board)) 
 		{ 
 			int value = 1000;
 			cur.setValue(value);
 			board[x][y] = 0;
 			return;
 		}
 		else if(depth > this.depthLimit)
 		{
 			int value = getBoardValue(board,player);
 			cur.setValue(value);
 			board[x][y] = 0;
 			return;
 		}
 		else { //Otherwise we loop through our possible child branches and send them on their way.
 			for(int i = 0; i < board.length; i++) {
 				int tmp = nextValidMoveInCol(board, i);
 				if(tmp != -1 && canPlacePiece(i,tmp,board)) {
 					generateTree(cur, i,tmp,!player,depth+1);
 				}
 			}
 		}

 		//Our branch is done, return up the tree. 
 		board[x][y] = 0;
 		return;
 	}
	
	/**
	 * 
	 * @param board 
	 * @return the value of the board
	 * @author Josh & Rahkeem
	 */
	private int getBoardValue(int[][] board, boolean player) {
		int playerNum = player ? 1 : 2;
		int otherPlayer = player ? 2 : 1;
		int possibleWins = 0;
		int enemyPossibleWins = 0;
		for(int x = 0; x < board.length; x++) {
			for(int y = 0; y < board[x].length; y++) {
				if(board[x][y] == playerNum) {
					possibleWins += calculatePossibleHorizontalWins(board, x, y, playerNum);
					possibleWins += calculatePossibleVerticalWins(board, x, y, playerNum);
					possibleWins += calculatePossibledDiagonalWins(board, x, y, playerNum);
				}
				else if(board[x][y] == otherPlayer) {
					enemyPossibleWins += calculatePossibleHorizontalWins(board, x, y, otherPlayer);
					enemyPossibleWins += calculatePossibleVerticalWins(board, x, y, otherPlayer);
					enemyPossibleWins += calculatePossibledDiagonalWins(board, x, y, otherPlayer);
				}
			}
		}

		return possibleWins - enemyPossibleWins;
	}
	
	
	/**
	 * 
	 * @param board
	 * @param x
	 * @param y
	 * @param player
	 * @return The score of the possible horizontal wins based on the piece at location x, y.
	 * @author Josh & Rahkeem
	 */
	private int calculatePossibleHorizontalWins(int[][] board, int x, int y, int player) {
		int tmp = 0;
		int otherPlayer = (player == 1 ? 2 : 1);
		for(int j = 3; j >= 0; j--) {
			int startX = x-j;
			int endX = 3 + startX;
			if(startX < 0 || endX >= board.length) 
				continue;
			int score = 1;
			for(int k = startX; k <= endX; k++) {
				if(board[k][y] == otherPlayer) {
					score = 0;
					break;
				}
				else if(board[k][y] == player && k != x) {
					score *= 4;
				}
			}
			tmp += score;
		}
		return tmp;
	}
	
	/**
	 * 
	 * @param board
	 * @param x
	 * @param y
	 * @param player
	 * @return The score of the possible vertical wins based on the piece at location x, y.
	 * @author Josh & Rahkeem
	 */
	private int calculatePossibleVerticalWins(int[][] board, int x, int y, int player) {
		int tmp = 0;
		int otherPlayer = (player == 1 ? 2 : 1);
		for(int j = 3;j >= 0; j-- ) {
			int startY = y - j;
			int endY = 3 + startY;
			if(startY < 0 || endY >= board[x].length) 
				continue;
			int score = 1;
			for(int k = startY; k <= endY; k++) {
				if(board[x][k] == otherPlayer) {
					score = 0;
					break;
				}
				else if(board[x][k] == player && k != y) {
					score *= 4;
				}
			}
			tmp += score;
		}
		return tmp;
	}

	/**
	 * 
	 * @param board
	 * @param x
	 * @param y
	 * @param player
	 * @return The score of the possible diagonal wins based on the piece at location x, y.
	 * @author Josh & Rahkeem
	 */
	private int calculatePossibledDiagonalWins(int[][] board, int x, int y, int player) {
		int tmp = 0;
		int otherPlayer = (player == 1 ? 2 : 1);
		//Diagonal \
		for(int j = 3; j >= 0; j--) {
			int startX = x-j, startY = y+j;
			int endX = startX+3, endY = startY-3;
			if(startX < 0 || startY >= board[x].length || endY < 0 || endX >= board.length)
				continue;
			int score = 1;
			for(int k = startX,l = startY; k <= endX && l >= endY; k++,l--) {
				if(board[k][l] == otherPlayer) {
					score = 0;
					break;
				}
				else if(board[k][l] == player && k != x && l != y) {
					score *= 4; 
				}
			}
			tmp += score;
		}
		
		//Diagonal /
		for(int j = 3; j >= 0; j--) {
			int startX = x-j, startY = y-j;
			int endX =startX+3, endY =  startY+3;
			if(startX <0 || startY < 0 || endX >= board.length || endY >= board[startX].length)
				continue;
			int score = 1;
			for(int k = startX, l=startY; k <= endX && l <= endY; k++,l++) {
				if(board[k][l] == otherPlayer) {
					score = 0;
					break;
				}
				else if(board[k][l] == player && k != x && l != y) {
					score *= 4; 
				}
			}
			tmp += score;
		}
		return tmp;
	}
	
	/**
	 * This method examines the current board[][] to see if the game is over. 
	 * A game is over if there are no more possible moves (i.e. the board is full) or
	 * if a player has won. 
	 * @return True if there is a winner or the board is full
	 * @author Josh Letcher
	 */
	private boolean isGameOver(int x, int y,int[][] board) {
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board[i].length; j++) {
				if(board[i][j] == 0)
					return false || hasWinner(x, y,board);
			}
		}
		return true;
	}

	/**
	 * This methods examines the current board[][] to see if there is a winner. 
	 * @return True if there is a player with four pieces in a row
	 * @author Scott Kieronski
	 */
	private boolean hasWinner(int x, int y, int[][] board) 
	{
		int playerVal = board[x][y]; //value of the piece that we just placed
		//if this is the starting piece, we just started the game, no winner 
		if(playerVal == 0)
		{
			return false;
		}
		
		//check for horizontal victory
		int winCounter = 0; //winCounter is used to check if there are enough pieces linked together
		for(int i = x + 1; i < board.length; i++)
		{
			if(board[i][y] == playerVal)
			{
				winCounter++;
				if(winCounter == 3)
				{
					return true;
				}
			}
			else
			{
				break;
			}
		}
		for(int i = x - 1; i >= 0; i--)
		{
			if(board[i][y] == playerVal)
			{
				winCounter++;
				if(winCounter == 3)
				{
					return true;
				}
			}
			else
			{
				break;
			}
		}
		
		//check for vertical victory
		winCounter = 0;
		for(int j = y - 1; j >= 0; j--)
		{
			if(board[x][j] == playerVal)
			{
				winCounter++;
				if(winCounter == 3)
				{
					return true;
				}
			}
			else
			{
				break;
			}
		}
		
		//check for diagonal (/) victory
		winCounter = 0;
		for(int i = x + 1, j=y + 1; i < board.length && j < board[0].length; i++, j++)
		{
			if(board[i][j] == playerVal)
			{
				winCounter++;
				if(winCounter == 3)
				{
					return true;
				}
			}
			else
			{
				break;
			}
		}
		for(int i = x - 1, j= y - 1; i >= 0 && j >= 0; i--, j--)
		{
			if(board[i][j] == playerVal)
			{
				winCounter++;
				if(winCounter == 3)
				{
					return true;
				}
			}
			else
			{
				break;
			}
		}
		
		//check for diagonal (\) victory
		winCounter = 0;
		for(int i = x - 1, j=y + 1; i >= 0 && j < board[0].length; i--, j++)
		{
			if(board[i][j] == playerVal)
			{
				winCounter++;
				if(winCounter == 3)
				{
					return true;
				}
			}
			else
			{
				break;
			}
		}
		for(int i = x + 1, j= y - 1; i < board.length && j >= 0; i++, j--)
		{
			if(board[i][j] == playerVal)
			{
				winCounter++;
				if(winCounter == 3)
				{
					return true;
				}
			}
			else
			{
				break;
			}
		}
		
		return false; //return false if this piece did not cause a winning configuration
	}
	
	/**
	 * 
	 * @param board
	 * @param x
	 * @return the y coordinate of the next valid move in the column specified by the x coordinate
	 * @author Josh
	 */
	private int nextValidMoveInCol(int[][] board, int x) {
		for(int i = board[x].length-1; i >= 0; i-- ) {
			if(canPlacePiece(x,i,board)) 
				return i;
		}
		return -1;
	}

	/**
	 * This method checks to see if placing a piece at location (x,y) is a valid move.
	 * @param x The x coordinate of the board
	 * @param y The y coordinate of the board
	 * @return If placing a piece in location (x,y) is a valid move.
	 * @author Josh
	 */
	private boolean canPlacePiece(int x, int y, int[][] board) {
		return (x < board.length && y < board[x].length ) && ( ( y == 0 && board[x][y] == 0) || (y != 0 && board[x][y-1] != 0 && board[x][y] == 0));
	}

}
