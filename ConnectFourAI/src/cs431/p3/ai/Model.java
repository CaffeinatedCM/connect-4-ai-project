/**
 * Contains the AI and Board to be saved and updated
 * 
 * @autor Mark Burton
 * 
 * Date: 03/18/2014
 * 
 */

package cs431.p3.ai;

import Controller.Controller;

public class Model
{
	private static final int PLAYER_1 = 1;
	private static final int PLAYER_2 = 2;
	public static final int ERROR = -1;
	
	/*Updated to have 2 instances of Connect 4
	 * This way, we can use any AI that extends the base class,
	 * not being stuck with specific children types
	 * 
	 * updated by Mark Burton*/ 
  private Connect4AI myPlayer1, myPlayer2;
  private Board myBoard;
  private Controller myController;
  
  private int myCurrentPlayer;	// Represents the player who must next take a turn
  /**
   * Constructor 
   * 
   * @param c,	Controller
   * @throws Exception 
   */
  public Model(Controller c) throws Exception
  {
  	//myPlayer1 = new Team1AI();
  	
  	myPlayer1 = new Team2AI(true);
  	myPlayer2 = new Team2AI(false);
  	myBoard = new Board();
  	
//  	myBoard.insert(4, Board.PLAYER_2);
//  	myBoard.insert(4, Board.PLAYER_2);
//  	myBoard.insert(4, Board.PLAYER_2);
//  	
//  	myBoard.insert(0, Board.PLAYER_2);
//  	
  	System.out.println(myBoard.toString());
  	
  	myController = c;
  }
  
  public Model()
  {
	  myBoard = new Board();
	  	
//	myBoard.insert(4, Board.PLAYER_2);
//	myBoard.insert(4, Board.PLAYER_2);
//	myBoard.insert(4, Board.PLAYER_2);
//	
//	myBoard.insert(0, Board.PLAYER_2);
//	
	System.out.println(myBoard.toString());
  }
  
  /**
   * Returns the grid of this class's instance of Board
   * 
   * @return int[][] the grid of this class's Board instance
   */
  public int[][] getBoard()
  {
  	return myBoard.getGrid();
  }
  
  /**
   * Sets the player who will place first
   * Then takes the turn
   * 
   * @param player	int	the first player to make a move
   */
  public void startMatch(int player)
  {
  	myCurrentPlayer = player;
  	System.out.println("current player: " + myCurrentPlayer);
  	//this.performTurn();
  }
  
/**
 *	Allows the current player to take a turn,
 *	then switches the player 
 */
	public void performTurn()
	{
		if(myCurrentPlayer == PLAYER_1) // If it is player 1's turn
		{
			System.out.println("Performing turn for player: " + PLAYER_1);
			int column = myPlayer1.getMove(myBoard.getGrid(), true);
//			System.out.println("ASKJDALKSJDSALKDJ");
			myBoard.insert(column, PLAYER_1);
			System.out.println("------------------------------------------------");
			myCurrentPlayer = PLAYER_2; // Switch players
		}
		else if(myCurrentPlayer == PLAYER_2)
		{
			System.out.println("Performing turn for player: " + PLAYER_2);
			int column = myPlayer2.getMove(myBoard.getGrid(), false);
			myBoard.insert(column, PLAYER_2);
			System.out.println("------------------------------------------------");
			myCurrentPlayer = PLAYER_1;	// Switch Players
		}
	}
	
	
}
