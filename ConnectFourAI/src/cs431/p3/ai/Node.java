package cs431.p3.ai;

import java.util.Vector;

/**
 * 
 * @author Katie Moore
 * Date Due: Mar. 19, 2014
 * Purpose: To hold the information needed in the tree.
 *
 */
public class Node 
{
	//////////////////////////
	//						//
	//       Properties		//
	//						//
	//////////////////////////
	
	// Constants
	public static int X_POS = 0;
	public static int Y_POS = 1;
	
	// Variables
	private Vector<Node> mChildren;
	private Node mParent;
	private int mValue;
	private int mX;
	private int mY;
	private int mId;
	private int mAlpha;
	private int mBeta;
	private int mPassId;
	
	
	//////////////////////////
	//						//
	//		Methods			//
	//						//
	//////////////////////////
	
	/**
	 * Constructor.  Parent is null.  Use for root node.
	 */
	public Node()
	{
		this(null);
	}
	
	/**
	 * Constructor.
	 * 
	 * @param parent The Node that is the parent of the current node.
	 */
	public Node(Node parent)
	{
		this.setParent(parent);
		mChildren = new Vector<Node>(7);
		this.setValue(-1);
		mX = -1;
		mY = -1;
		mAlpha = Integer.MIN_VALUE;
		mBeta = Integer.MAX_VALUE;
	}
	
	public void setId(int id)
	{
		mId = id;
	}
	
	public int getId()
	{
		return mId;
	}
	
	public void setPassId(int id)
	{
		mPassId = id;
	}
	
	public int getPassId()
	{
		return mPassId;
	}
	
	public void setAlpha(int val)
	{
		mAlpha = val;
	}
	
	public int getAlpha()
	{
		return mAlpha;
	}
	
	public void setBeta(int val)
	{
		mBeta = val;
	}
	
	public int getBeta()
	{
		return mBeta;
	}
	/**
	 * Set the Heuristic value of the node.
	 * 
	 * @param value The value of the Node.
	 */
	public void setValue(int value)
	{
		mValue = value;
	}
	
	/**
	 * Set the Parent node of the current node.
	 * 
	 * @param node The parent of the Node.
	 */
	public void setParent(Node node)
	{
		mParent = node;
	}
	
	/**
	 * Adds a child associated with the current node.
	 * Call once for each child to add.
	 * 
	 * @param child The child object Node.
	 */
	public void setChild(Node child)
	{
		mChildren.add(child);
	}
	
	/**
	 * Set the index in the board array the Node is in.
	 * 
	 * @param x The x index.
	 * @param y	The y index.
	 */
	public void setPosition(int x, int y)
	{
		mX = x;
		mY = y;
	}
	
	/**
	 * Returns a Vector of the current Node's children.
	 * @return A vector of the current Node's children.
	 */
	public Vector<Node> getChildren()
	{
		return mChildren;
	}
	
	/**
	 * Returns the Heuristic value of the current Node.
	 * @return The heuristic value of the current Node.
	 */
	public int getValue()
	{
		return mValue;
	}
	
	/**
	 * Returns the x index of the current Node in the array.
	 * @return The x index of the current Node in the array.
	 */
	public int getX()
	{
		return mX;
	}
	
	/**
	 * Returns the y index of the current Node in the array.
	 * @return The y index of the current Node in the array.
	 */
	public int getY()
	{
		return mY;
	}

	/**
	 * Returns the x and y indices of the current Node as an Integer array.  
	 * Use constants Node.X_POS to access the x index, and Node.Y_POS to access the y index.
	 * @return The x and y indices of the current Node as an Integer array.
	 */
	public int[] getPosition()
	{
		int[] pos = {mX, mY};
		return pos;
	}
	
	public Node getParent()
	{
		return mParent;
	}
	
	public String toString()
	{
		return "ID: " + mId + " Alpha: "+ mAlpha + " Beta: " + mBeta + " Value: " + mValue + " PassID: " + mPassId + " X: " + mX + " Y: " + mY;
	}
}
