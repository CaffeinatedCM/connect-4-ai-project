package cs431.p3.ai;

/**
 * Interface for which an agent for the Connect 4 game must implement in order to function.
 * @author Nathan Hilliard
 */
public abstract class Connect4AI {
	/**
	 * Function must return the new state of the board following its decided move.
	 * @param board The current state of the game board.
	 * @return The column the AI made a move in.
	 */
	public abstract int getMove(int[][] board, boolean currentPlayer);
}