/**
 * Represents a Connect 4 Board
 * 
 * @author Mark Burton
 * 
 * Date: 03/18/2014
 */

package cs431.p3.ai;

public class Board
{
	// Player values
	public final static int BLANK = 0;
	public final static int PLAYER_1 = 1;
	public final static int PLAYER_2 = 2;
	
	// Board values
	public final static int HEIGHT = 6;
	public final static int WIDTH = 7;
	
	// The double dimensional array representation of the grid
	private int[][] myGrid;
	
	/**
	 *  Default Constructor
	 */
	
	public Board()
	{
		myGrid = new int[WIDTH][HEIGHT];
		initBlankBoard();
	}
	
	/**
	 * Constructor 
	 *  
	 * @param board	int[][] the grid with the set of values to be filled.
	 */
	
	public Board(int[][] board)
	{
		myGrid = new int[WIDTH][HEIGHT];
		initSetBoard(board);
	}
	
	/**
	 * Initializes the grid to have the same values as the passed in board
	 * 
	 * @param board	int[][] the arrays filled with the desired value
	 */
	
	public void initSetBoard(int[][] board)
	{
		for(int x = 0; x < WIDTH; x++)
			for(int y = 0; y < HEIGHT; y++)
			{
				myGrid[x][y] = board[x][y];
			}
	}
	
	/**
	 * 
	 * Initiates all the values in this objects grid to being BLANK
	 */
	
	private void initBlankBoard()
	{
		for(int x = 0; x < WIDTH; x++)
			for(int y = 0; y < HEIGHT; y++)
			{
				myGrid[x][y] = BLANK;
			}
	}
	
	/**
	 * Inserts a new piece to the board, sliding down to the next available place in the column
	 * 
	 * @param column	int, the column the player wishes to slide their piece down
	 * @param pieceType	int, the player the piece belongs to
	 * 
	 * @return boolean, 	true if insert was successful,
	 * 										false if insert was unsuccessful
	 */
	public boolean insert(int column, int pieceType)
	{
     if((column < 0)|| (column >= WIDTH  )) // if this column does not exist
     {
    	 System.out.println("COLUMN DOESN'T EXIST");

    	 return false;
     }
     else if(myGrid[column][0] != BLANK) // if this space is not available
     {
    	 System.out.println("SPACE NOT AVAILABLE");
    	 return false;
     }

     // Perform the slide down operation
     System.out.println("ASLKDJASLKDJSALDKJ");
     int i = HEIGHT - 1; // start the incrementing at the bottom of the board
     boolean placed = false; // determines if the piece was placed
     
     System.out.println("i : " + i);
     while((i != -1)&&(!placed))
     {
    	 System.out.println("Reached");
    	 System.out.println("PLAEYER: "+myGrid[column][i]);
    	 if(myGrid[column][i] == BLANK) // if this spot is open
    	 {
    		 System.out.println("SPOT IS OPEN");
    		 myGrid[column][i] = pieceType; // insert it
    		 placed = true;
    	 }
    	 
    	 i--;
     }
     
     return true;
	}
	
  /**
   * Returns the board
   * 
   * @return myGrid, int[][] representing the board
   */
	
	public int[][] getGrid()
	{
		return myGrid;
	}
	
	public String toString()
	{
		String board = "";
		
//		for(int x = 0; x < WIDTH; x++)
//		{
//			for(int y = 0; y < HEIGHT; y++)
//			{
//				if(myGrid[x][y] == BLANK)
//				  board += " * ("+x+","+y+")";
//				else if(myGrid[x][y] == PLAYER_2)
//					board += " R ("+x+","+y+")";
//				else if(myGrid[x][y] == PLAYER_2)
//					board += " B ("+x+","+y+")";
//			}
//			board += "\n";
//	  }
		
		for(int y = 0; y < HEIGHT; y++)
		{
			for(int x = 0; x < WIDTH; x++)
			{
				if(myGrid[x][y] == BLANK)
				  board += " * ("+x+","+y+")";
				else if(myGrid[x][y] == PLAYER_1)
					board += " 1 ("+x+","+y+")";
				else if(myGrid[x][y] == PLAYER_2)
					board += " 2 ("+x+","+y+")";
			}
			board += "\n";
	  }
	
		return board;
	}

}
