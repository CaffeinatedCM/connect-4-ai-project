package cs431.p3.ai;

/**
 * 
 * @author Katie Moore
 * Date Due: Mar. 24, 2013
 * Purpose: To perform Alpha Beta Pruning on a set of nodes.
 *
 */
public class AlphaBetaPruning 
{
	// Constants
	public static final int MAX = 0;
	public static final int MIN = 1;
	
	// Variables
	private Node bestNode = null;
	private int bestId = -1;
	
	/**
	 * Runs the Alpha Beta Pruning algorithm to find the most advantageous move to take.
	 * @param node The Root node of the tree.
	 * @param alpha	The alpha value.
	 * @param beta	The beta value.
	 * @param max_or_min Constant denoting whether to look for the Min or Max value.
	 * @return The value of the best cost.
	 */
	public Node alphaBeta(Node node, int alpha, int beta, int max_or_min)
	{	
		// Check if the node is a leaf node
		if(node.getChildren().size() == 0)
		{
			return node;
		}
		else
		{
			switch(max_or_min)
			{
			// If we are looking for the best move for the Max player
			case MAX:
				node.setAlpha(alpha);
				node.setBeta(beta);
				for(int i=0; i<node.getChildren().size(); i++)
				{
					Node tmp = alphaBeta(node.getChildren().get(i), node.getAlpha(), node.getBeta(), MIN);
					
					node.setAlpha(Math.max(node.getAlpha(), tmp.getValue()));
					
					if(node.getAlpha() == tmp.getValue())
					{
						bestNode = tmp;
						node.setValue(tmp.getValue());
						node.setPassId(tmp.getPassId());
						node.setPosition(tmp.getX(), tmp.getY());
					}
					
					if(node.getBeta() <= node.getAlpha())
					{
						break;
					}			
				}
				return node;
			
			// If we are looking for the best move for the Min player
			case MIN:
				node.setAlpha(alpha);
				node.setBeta(beta);
				for(int i=0; i<node.getChildren().size(); i++)
				{
					Node tmp = alphaBeta(node.getChildren().get(i), node.getAlpha(), node.getBeta(), MAX);
					
					node.setBeta(Math.min(node.getBeta(), tmp.getValue()));
					
					if(node.getBeta() == tmp.getValue())
					{
						bestNode = tmp;
						node.setValue(tmp.getValue());
						node.setPassId(tmp.getPassId());
						node.setPosition(tmp.getX(), tmp.getY());
					}
					
					if(node.getBeta() <= node.getAlpha())
						break;
				}
				return node;
				
				default: 
					return null;
			}		
		}
	}
	
	/** 
	 * main class for testing.  Answer should be node:26 value:6
	 */
//	public static void main(String[] args)
//	{
//		AlphaBetaPruning ab = new AlphaBetaPruning();
//		
//		Node[] nodes = new Node[33];
//		
//		for(int i=0; i<nodes.length; i++)
//		{
//			nodes[i] = new Node();
//			nodes[i].setId(i);
//			nodes[i].setPassId(i);
//			nodes[i].setPosition(i, i);
//		}
//		
//		nodes[0].setValue(Integer.MIN_VALUE);
//		nodes[1].setValue(Integer.MAX_VALUE);
//		nodes[2].setValue(Integer.MAX_VALUE);
//		nodes[3].setValue(Integer.MAX_VALUE);
//		nodes[4].setValue(Integer.MIN_VALUE);
//		nodes[5].setValue(Integer.MIN_VALUE);
//		nodes[6].setValue(Integer.MIN_VALUE);
//		nodes[7].setValue(Integer.MIN_VALUE);
//		nodes[8].setValue(Integer.MIN_VALUE);
//		nodes[9].setValue(Integer.MIN_VALUE);
//		nodes[10].setValue(Integer.MAX_VALUE);
//		nodes[11].setValue(Integer.MAX_VALUE);
//		nodes[12].setValue(Integer.MAX_VALUE);
//		nodes[13].setValue(Integer.MAX_VALUE);
//		nodes[14].setValue(Integer.MAX_VALUE);
//		nodes[15].setValue(Integer.MAX_VALUE);
//		nodes[16].setValue(Integer.MAX_VALUE);
//		nodes[17].setValue(Integer.MAX_VALUE);
//		nodes[18].setValue(Integer.MAX_VALUE);
//		nodes[19].setValue(5);
//		nodes[20].setValue(6);
//		nodes[21].setValue(7);
//		nodes[22].setValue(4);
//		nodes[23].setValue(5);
//		nodes[24].setValue(3);
//		nodes[25].setValue(6);
//		nodes[26].setValue(6);
//		nodes[27].setValue(9);
//		nodes[28].setValue(7);
//		nodes[29].setValue(5);
//		nodes[30].setValue(20);
//		nodes[31].setValue(8);
//		nodes[32].setValue(25);
//		
//		// set Connections
//		nodes[0].setChild(nodes[1]);
//		nodes[0].setChild(nodes[2]);
//		nodes[0].setChild(nodes[3]);
//		
//		nodes[1].setChild(nodes[4]);
//		nodes[1].setChild(nodes[5]);
//		
//		nodes[2].setChild(nodes[6]);
//		nodes[2].setChild(nodes[7]);
//		
//		nodes[3].setChild(nodes[8]);
//		nodes[3].setChild(nodes[9]);
//		
//		nodes[4].setChild(nodes[10]);
//		nodes[4].setChild(nodes[11]);
//		
//		nodes[5].setChild(nodes[12]);
//		
//		nodes[6].setChild(nodes[13]);
//		nodes[6].setChild(nodes[14]);
//		
//		nodes[7].setChild(nodes[15]);
//		
//		nodes[8].setChild(nodes[16]);
//		
//		nodes[9].setChild(nodes[17]);
//		nodes[9].setChild(nodes[18]);
//		
//		nodes[10].setChild(nodes[19]);
//		nodes[10].setChild(nodes[20]);
//		
//		nodes[11].setChild(nodes[21]);
//		nodes[11].setChild(nodes[22]);
//		nodes[11].setChild(nodes[23]);
//		
//		nodes[12].setChild(nodes[24]);
//		
//		nodes[13].setChild(nodes[25]);
//		
//		nodes[14].setChild(nodes[26]);
//		nodes[14].setChild(nodes[27]);
//		
//		nodes[15].setChild(nodes[28]);
//		
//		nodes[16].setChild(nodes[29]);
//		
//		nodes[17].setChild(nodes[30]);
//		nodes[17].setChild(nodes[31]);
//		
//		nodes[18].setChild(nodes[32]);
//		
//		System.out.println(ab.alphaBeta(nodes[0], nodes[0].getAlpha(), nodes[0].getBeta(), AlphaBetaPruning.MAX));
//	}
		
}
