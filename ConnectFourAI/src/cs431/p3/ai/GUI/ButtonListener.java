package cs431.p3.ai.GUI;

import java.awt.event.*;
import java.lang.reflect.*;

/**
 * This class will handle button-related events and invoke a provided method
 * when an event is triggered.
 * @author Jure Jumalon
 */
public class ButtonListener extends MouseAdapter {
	/** Our instance of the controller. */
	private Object myController;
	
	/** An object representing the method that will be invoked. */
	private Method myMethod;
	
	/** The arguments to be passed to the method. */
	private Object[] myArgs;
	
	/**
	 * Specific constructor.
	 * @param controller An instance of the controller.
	 * @param method The method to invoke.
	 * @param args The arguments to pass to the method.
	 */
	public ButtonListener(Object controller, Method method, Object[] args) {
		this.myController = controller;
		this.myMethod = method;
		this.myArgs = args;
	}
	
	/**
	 * Function that runs the Method when the mouse is released after clicking a button.
	 */
	public void mouseReleased(MouseEvent e) {
		Method method;
		Object controller;
		Object[] arguments;
		
		method = this.getMethod();
		controller = this.getController();
		arguments = this.getArguments();
		
		try {
			method.invoke(controller, arguments);
		}
		catch(InvocationTargetException error) {
			System.out.println(error.getMessage());
		}
		catch(IllegalArgumentException error) {
			System.out.println(error.getMessage());
		}
		catch(IllegalAccessException error) {
			System.out.println(error.getMessage());
		}
	}
	
	/**
	 * @param args New arguments
	 */
	public void setArguments(Object[] args) {
		this.myArgs = args;
	}
	
	/**
	 * @param controller A new instance of the controller.
	 */
	public void setController(Object controller) {
		this.myController = controller;
	}
	
	/**
	 * @param method A new method object.
	 */
	public void setMethod(Method method) {
		this.myMethod = method;
	}
	
	/**
	 * @return The method.
	 */
	public Method getMethod() {
		return this.myMethod;
	}
	
	/**
	 * @return The methods parameters.
	 */
	public Object[] getArguments() {
		return this.myArgs;
	}
	
	/**
	 * @return The controller.
	 */
	public Object getController() {
		return this.myController;
	}
}