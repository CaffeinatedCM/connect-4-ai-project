/**
 * This is the GUI for the connect four AI project. A lot of love went into the look and design of this GUI. <3
 * @author Kyle Campbell & Jure Jumalon
 */

package cs431.p3.ai.GUI;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridLayout;

import javax.swing.JLabel;

import java.awt.Color;

import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.ImageIcon;
import javax.swing.BoxLayout;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import java.awt.Font;
import java.lang.reflect.Method;

import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import Controller.Controller;
import cs431.p3.ai.Board;
import cs431.p3.ai.Model;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

/**
 * The view component to the application. Handles all Graphical User Interface operations
 * to get input.
 * @author Jure Jumalon & Kyle Campbell
 */
public class Connect4GUI extends JFrame {
	/** The main panel. */
	private JPanel myContentPane;
	
	/** Our instance of the controller portion of the application. */
	private Controller myController;
	
	/** ComboBox for picking who will start the game. */
	private JComboBox myPlayerStart;
	
	/** Buttons for starting the game and making the next move. */
	private JButton myStartGameButton, myNextMoveButton;
	
	/** The buttons that allow a human player to input moves. */
	private JButton [] myColumnButtons;
	
	/** The JLabel representation of a cell on the game board. */
	private JLabel [][] myCells;
	
	/** The "log" which is used to display console-like output. */
	private JTextArea myLog;
	
	/** Path to images */
	private final String EMPTY_PATH = "White.png";
	private final String BLACK_PATH = "Black.png";
	private final String RED_PATH = "Red.png";


	/**
	 * Constructor that creates the frame and organizes all of the elements on it.
	 */
	public Connect4GUI(Controller c) {
		this.myController = c;
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(100, 100, 986, 663);
		this.myContentPane = new JPanel();
		this.myContentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(this.myContentPane);
		this.myContentPane.setLayout(new BoxLayout(this.myContentPane, BoxLayout.X_AXIS));
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBackground(new Color(234,234,0));
		this.myContentPane.add(panel);
		panel.setLayout(new GridLayout(7, 7, 0, 0));
		
		this.myColumnButtons = new JButton[Board.WIDTH];
		for (int i = 0; i < Board.WIDTH; i++) {
			this.myColumnButtons[i] = new JButton("Column " + i);
			panel.add(this.myColumnButtons[i]);
		}
		
		this.myCells = new JLabel[Board.HEIGHT][Board.WIDTH];
		for (int i = 0; i < Board.HEIGHT; i++) {
			for (int j = 0; j < Board.WIDTH; j++) {
				this.myCells[i][j] = new JLabel("");
				this.myCells[i][j].setIcon(new ImageIcon(Connect4GUI.class.getResource(EMPTY_PATH)));
				this.myCells[i][j].setHorizontalAlignment(SwingConstants.CENTER);
				panel.add(this.myCells[i][j]);
			}
		}
		
		JPanel panel_1 = new JPanel();
		this.myContentPane.add(panel_1);
		
		// Create buttons
		this.myStartGameButton = new JButton("Start Game");
		this.myStartGameButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		this.myNextMoveButton = new JButton("Make Next Move");
		this.myNextMoveButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		// Associate listeners to the buttons
		this.associateListeners();

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		this.myPlayerStart = new JComboBox();
		this.myPlayerStart.setModel(new DefaultComboBoxModel(new String[] {"Player 1 goes first", "Player 2 goes first"}));
		this.myPlayerStart.setMaximumRowCount(2);
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 320, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(Alignment.LEADING, gl_panel_1.createSequentialGroup()
					.addGap(44)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addComponent(this.myNextMoveButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGap(41))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(this.myPlayerStart, Alignment.LEADING, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(this.myStartGameButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 259, Short.MAX_VALUE))
							.addContainerGap())))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(17)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 349, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
					.addComponent(this.myPlayerStart, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(this.myStartGameButton, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(this.myNextMoveButton, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		
		this.myLog = new JTextArea();
		this.myLog.setLineWrap(true);
		scrollPane.setViewportView(this.myLog);
		panel_1.setLayout(gl_panel_1);
		
	}

	/**
	 * Function to associate all the objects on the view with the listener using reflection.
	 */
	public void associateListeners()
	{
		String error;
		Class<? extends Controller> controllerClass = this.myController.getClass();
		Method startGameMethod = null;
		Method nextMoveMethod = null;
		Method[] makeMoveMethod = new Method[Board.WIDTH];
		
		try {
			startGameMethod = controllerClass.getMethod("startGame", (Class<?>[]) null);
			nextMoveMethod = controllerClass.getMethod("nextMove", (Class<?>[]) null);
			
			for (int i = 0; i < Board.WIDTH; i++) {
				Class<?>[] cArg = new Class[1];
				cArg[0] = Integer.class;
				makeMoveMethod[i] = controllerClass.getMethod("makeMove", cArg);
			}
		}
		catch (SecurityException e) {
			error = e.getMessage();
			System.out.println(error);
		}
		catch (NoSuchMethodException e) {
			error = e.getMessage();
			System.out.println(error);
		}
		
		ButtonListener startGameListener = new ButtonListener(this.myController, startGameMethod, null);
		ButtonListener nextMoveListener = new ButtonListener(this.myController, nextMoveMethod, null);
		ButtonListener[] makeMoveListener = new ButtonListener[Board.WIDTH];
		for (int i = 0; i < Board.WIDTH; i++) {
			Object[] args = new Object[1];
			args[0] = new Integer(i);
			makeMoveListener[i] = new ButtonListener(this.myController, makeMoveMethod[i], args);
		}
		
		this.myStartGameButton.addMouseListener(startGameListener);
		this.myNextMoveButton.addMouseListener(nextMoveListener);
		
		for (int i = 0; i < Board.WIDTH; i++)
			this.myColumnButtons[i].addMouseListener(makeMoveListener[i]);
	}
	/**
	 * Determines which player was selected to go first.
	 * @return the starting player.
	 */
	public int getStartingPlayer() {
		if (this.myPlayerStart.getSelectedIndex() == 0)
			return Board.PLAYER_1;
		else if (this.myPlayerStart.getSelectedIndex() == 1)
			return Board.PLAYER_2;
		else
			return Model.ERROR;
	}
	/**
	 * This method updates the GUI with the latest move that the player made.
	 * @param i The column of the board
	 * @param j The Row of the board
	 * @param player The current player that made the move.
	 */
	public void updateCell(int i, int j, int player) {
		if (player == Board.PLAYER_1)
			this.myCells[i][j].setIcon(new ImageIcon(Connect4GUI.class.getResource(RED_PATH)));
		else if (player == Board.PLAYER_2)
			this.myCells[i][j].setIcon(new ImageIcon(Connect4GUI.class.getResource(BLACK_PATH)));
		else
			this.myCells[i][j].setIcon(new ImageIcon(Connect4GUI.class.getResource(EMPTY_PATH)));
	}
	/**
	 * Appends a message to the end of the play mLog.
	 * @param msg The string to append to the play mLog
	 */
	public void updateLog(String msg) {
		this.myLog.append(msg + "\n");
	}
}
