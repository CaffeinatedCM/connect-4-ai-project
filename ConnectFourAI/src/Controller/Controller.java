package Controller;

import cs431.p3.ai.Model;
import cs431.p3.ai.GUI.Connect4GUI;

//import cs431.p3.model.Board;
//import cs431.p3.model.Model;
//import cs431.p3.GUI.*;

/**
 * The controller component of the application.
 * @author Nathan Hilliard, Kyle Campbell, and Jure Jumalon
 * 
 * updated: 03/18/2014 by @author Mark Burton
 * 	- Revisions: Updated update method to adjust between differing coordinate systems
 */
public class Controller {
	/** Instance of the model. */
	private Model myModel;
	private Connect4GUI frame;
	
	/**
	 * Default constructor.
	 * @throws Exception 
	 */
	public Controller() throws Exception {
		this.myModel = new Model(this);
		this.frame = new Connect4GUI(this);
		frame.setVisible(true);
		//update();
	}
	/**
	 * Starts the game with the selected player going first.
	 */
	public void startGame()
	{
		int player = this.frame.getStartingPlayer();
		this.myModel.startMatch(player);
	} 
	/**
	 * Makes the next move and then updates the GUI.
	 */
	public void nextMove()
	{
		this.myModel.performTurn();
		update();
	}
	
	/**
	 *This simply takes in the current board state and updates the GUI with the correct information. */
	public void update()
	{
		
		int[][] board = this.myModel.getBoard();
		for(int i = 0; i < board.length; i++)
		{
			for(int j = 0; j < board[i].length; j++)
			{
				this.frame.updateCell(j, i, board[i][j]); // changed to match with the GUI coordinates, by Mark Burton
				//this.frame.updateCell(i, j, board[i][j]);
				//System.out.println("I: "+i+", J: "+j);
				//System.out.println("BOARD "+board[i][j]);
			}
		}
		
	}
	/**
	 * Prints out the last move that was made and by who.
	 */
	public void printLog()
	{
		//this.myModel.getBoard().toString();
	}
}
